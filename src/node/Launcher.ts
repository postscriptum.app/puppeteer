/**
 * Copyright 2017 Google Inc. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import * as os from 'os';
import * as path from 'path';
import * as fs from 'fs';

import { assert } from '../common/assert.js';
import { Browser } from '../common/Browser.js';
import { BrowserRunner } from './BrowserRunner.js';
import { promisify } from 'util';

const copyFileAsync = promisify(fs.copyFile);
const mkdtempAsync = promisify(fs.mkdtemp);

import {
  BrowserLaunchArgumentOptions,
  ChromeReleaseChannel,
  PuppeteerNodeLaunchOptions,
} from './LaunchOptions.js';

import { Product } from '../common/Product.js';

const tmpDir = () => process.env.PUPPETEER_TMP_DIR || os.tmpdir();

/**
 * Describes a launcher - a class that is able to create and launch a browser instance.
 * @public
 */
export interface ProductLauncher {
  launch(object: PuppeteerNodeLaunchOptions): Promise<Browser>;
  executablePath: (path?: any) => string;
  defaultArgs(object: BrowserLaunchArgumentOptions): string[];
  product: Product;
}

/**
 * @internal
 */
class ChromeLauncher implements ProductLauncher {
  _projectRoot: string | undefined;
  _preferredRevision: string;
  _isPuppeteerCore: boolean;

  constructor(
    projectRoot: string | undefined,
    preferredRevision: string,
    isPuppeteerCore: boolean
  ) {
    this._projectRoot = projectRoot;
    this._preferredRevision = preferredRevision;
    this._isPuppeteerCore = isPuppeteerCore;
  }

  async launch(options: PuppeteerNodeLaunchOptions = {}): Promise<Browser> {
    const {
      ignoreDefaultArgs = false,
      args = [],
      dumpio = false,
      channel = null,
      executablePath = null,
      pipe = false,
      env = process.env,
      handleSIGINT = true,
      handleSIGTERM = true,
      handleSIGHUP = true,
      ignoreHTTPSErrors = false,
      defaultViewport = { width: 800, height: 600 },
      slowMo = 0,
      timeout = 30000,
      waitForInitialPage = true,
      debuggingPort = null,
    } = options;

    const chromeArguments = [];
    if (!ignoreDefaultArgs) chromeArguments.push(...this.defaultArgs(options));
    else if (Array.isArray(ignoreDefaultArgs))
      chromeArguments.push(
        ...this.defaultArgs(options).filter(
          (arg) => !ignoreDefaultArgs.includes(arg)
        )
      );
    else chromeArguments.push(...args);

    if (
      !chromeArguments.some((argument) =>
        argument.startsWith('--remote-debugging-')
      )
    ) {
      if (pipe) {
        assert(
          debuggingPort === null,
          'Browser should be launched with either pipe or debugging port - not both.'
        );
        chromeArguments.push('--remote-debugging-pipe');
      } else {
        chromeArguments.push(`--remote-debugging-port=${debuggingPort || 0}`);
      }
    }

    let userDataDir;
    let isTempUserDataDir = true;

    // Check for the user data dir argument, which will always be set even
    // with a custom directory specified via the userDataDir option.
    const userDataDirIndex = chromeArguments.findIndex((arg) => {
      return arg.startsWith('--user-data-dir');
    });

    if (userDataDirIndex !== -1) {
      userDataDir = chromeArguments[userDataDirIndex].split('=')[1];
      isTempUserDataDir = false;
    } else {
      userDataDir = await mkdtempAsync(
        path.join(tmpDir(), 'puppeteer_dev_chrome_profile-')
      );
      chromeArguments.push(`--user-data-dir=${userDataDir}`);
    }

    let chromeExecutable = executablePath;

    if (channel) {
      // executablePath is detected by channel, so it should not be specified by user.
      assert(
        !executablePath,
        '`executablePath` must not be specified when `channel` is given.'
      );

      chromeExecutable = executablePathForChannel(channel);
    } else if (!executablePath) {
      throw new Error('`executablePath` must be specified');
    }

    const usePipe = chromeArguments.includes('--remote-debugging-pipe');
    const runner = new BrowserRunner(
      this.product,
      chromeExecutable,
      chromeArguments,
      userDataDir,
      isTempUserDataDir
    );
    runner.start({
      handleSIGHUP,
      handleSIGTERM,
      handleSIGINT,
      dumpio,
      env,
      pipe: usePipe,
    });

    let browser;
    try {
      const connection = await runner.setupConnection({
        usePipe,
        timeout,
        slowMo,
        preferredRevision: this._preferredRevision,
      });
      browser = await Browser.create(
        connection,
        [],
        ignoreHTTPSErrors,
        defaultViewport,
        runner.proc ?? undefined,
        runner.close.bind(runner)
      );
    } catch (error) {
      runner.kill();
      throw error;
    }

    if (waitForInitialPage) {
      try {
        await browser.waitForTarget((t) => t.type() === 'page', { timeout });
      } catch (error) {
        await browser.close();
        throw error;
      }
    }

    return browser;
  }

  defaultArgs(options: BrowserLaunchArgumentOptions = {}): string[] {
    const chromeArguments = [
      '--disable-background-networking',
      '--enable-features=NetworkService,NetworkServiceInProcess',
      '--disable-background-timer-throttling',
      '--disable-backgrounding-occluded-windows',
      '--disable-breakpad',
      '--disable-client-side-phishing-detection',
      '--disable-component-extensions-with-background-pages',
      '--disable-default-apps',
      '--disable-dev-shm-usage',
      '--disable-extensions',
      '--disable-features=Translate',
      '--disable-hang-monitor',
      '--disable-ipc-flooding-protection',
      '--disable-popup-blocking',
      '--disable-prompt-on-repost',
      '--disable-renderer-backgrounding',
      '--disable-sync',
      '--force-color-profile=srgb',
      '--metrics-recording-only',
      '--no-first-run',
      '--enable-automation',
      '--password-store=basic',
      '--use-mock-keychain',
      // TODO(sadym): remove '--enable-blink-features=IdleDetection'
      // once IdleDetection is turned on by default.
      '--enable-blink-features=IdleDetection',
      '--export-tagged-pdf',
    ];
    const {
      devtools = false,
      headless = !devtools,
      args = [],
      userDataDir = null,
    } = options;
    if (userDataDir)
      chromeArguments.push(`--user-data-dir=${path.resolve(userDataDir)}`);
    if (devtools) chromeArguments.push('--auto-open-devtools-for-tabs');
    if (headless) {
      chromeArguments.push('--headless', '--hide-scrollbars', '--mute-audio');
    }
    if (args.every((arg) => arg.startsWith('-')))
      chromeArguments.push('about:blank');
    chromeArguments.push(...args);
    return chromeArguments;
  }

  executablePath(channel?: ChromeReleaseChannel): string {
    if (channel) {
      return executablePathForChannel(channel);
    } else {
      throw new Error('`executablePath()` not implemented');
    }
  }

  get product(): Product {
    return 'chrome';
  }
}

function executablePathForChannel(channel: ChromeReleaseChannel): string {
  const platform = os.platform();

  let chromePath: string | undefined;
  switch (platform) {
    case 'win32':
      switch (channel) {
        case 'chrome':
          chromePath = `${process.env.PROGRAMFILES}\\Google\\Chrome\\Application\\chrome.exe`;
          break;
        case 'chrome-beta':
          chromePath = `${process.env.PROGRAMFILES}\\Google\\Chrome Beta\\Application\\chrome.exe`;
          break;
        case 'chrome-canary':
          chromePath = `${process.env.PROGRAMFILES}\\Google\\Chrome SxS\\Application\\chrome.exe`;
          break;
        case 'chrome-dev':
          chromePath = `${process.env.PROGRAMFILES}\\Google\\Chrome Dev\\Application\\chrome.exe`;
          break;
      }
      break;
    case 'darwin':
      switch (channel) {
        case 'chrome':
          chromePath =
            '/Applications/Google Chrome.app/Contents/MacOS/Google Chrome';
          break;
        case 'chrome-beta':
          chromePath =
            '/Applications/Google Chrome Beta.app/Contents/MacOS/Google Chrome Beta';
          break;
        case 'chrome-canary':
          chromePath =
            '/Applications/Google Chrome Canary.app/Contents/MacOS/Google Chrome Canary';
          break;
        case 'chrome-dev':
          chromePath =
            '/Applications/Google Chrome Dev.app/Contents/MacOS/Google Chrome Dev';
          break;
      }
      break;
    case 'linux':
      switch (channel) {
        case 'chrome':
          chromePath = '/opt/google/chrome/chrome';
          break;
        case 'chrome-beta':
          chromePath = '/opt/google/chrome-beta/chrome';
          break;
        case 'chrome-dev':
          chromePath = '/opt/google/chrome-unstable/chrome';
          break;
      }
      break;
  }

  if (!chromePath) {
    throw new Error(
      `Unable to detect browser executable path for '${channel}' on ${platform}.`
    );
  }

  // Check if Chrome exists and is accessible.
  try {
    fs.accessSync(chromePath);
  } catch (error) {
    throw new Error(
      `Could not find Google Chrome executable for channel '${channel}' at '${chromePath}'.`
    );
  }

  return chromePath;
}

/**
 * @internal
 */
export default function Launcher(
  projectRoot: string | undefined,
  preferredRevision: string,
  isPuppeteerCore: boolean,
  product?: string
): ProductLauncher {
  // puppeteer-core doesn't take into account PUPPETEER_* env variables.
  if (!product && !isPuppeteerCore)
    product =
      process.env.PUPPETEER_PRODUCT ||
      process.env.npm_config_puppeteer_product ||
      process.env.npm_package_config_puppeteer_product;
  switch (product) {
    case 'chrome':
    default:
      if (typeof product !== 'undefined' && product !== 'chrome') {
        /* The user gave us an incorrect product name
         * we'll default to launching Chrome, but log to the console
         * to let the user know (they've probably typoed).
         */
        console.warn(
          `Warning: unknown product name ${product}. Falling back to chrome.`
        );
      }
      return new ChromeLauncher(
        projectRoot,
        preferredRevision,
        isPuppeteerCore
      );
  }
}
